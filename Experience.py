class Experience(object):

    def __init__(self, year=None, working_environment=None, position=None,
            body_text=None):

        self.year = year
        self.working_environment = working_environment
        self.position = position
        self.body_text = body_text
