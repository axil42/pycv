class ContactInfo(object):

    def __init__(self, name=None, address=None, telephone=None, mobile=None,
        email=None):
       
        self.name = name
        self.address = address
        self.telephone = telephone
        self.mobile = mobile
        self.email = email

    def __repr__(self):
        pass


class Address(object):
    
    def __init__(self, street_name=None, street_number=None, region=None,
            postal_code=None, city=None, country=None):

        self.street_name = str(street_name)
        self.street_number = str(street_number)
        self.region = str(region)
        self.postal_code = str(postal_code)
        self.city = str(city)
        self.country = str(country)
        self.lines = repr(self).split('\n')

    def __repr__(self):
        return """{0} {1}\n{2}, {3}\n{4}, {5}\n""".format(self.street_number, 
                self.street_name, self.region, self.postal_code, self.city, self.country)

class Name(object):

    def __init__(self, first_name=None, last_name=None):

       self.first_name = str(first_name) 
       self.last_name = str(last_name)
    
    def __repr__(self):
        
        return "{0} {1}".format(self.first_name, self.last_name)

class Phone(object):
    
    def __init__(self, country_prefix=None, phone_number=None):
        
        self.country_prefix = str(country_prefix)
        self.phone_number = str(phone_number)

    def __repr__(self):
        
        return "{0}-{1}".format(self.country_prefix, self.phone_number)

class Email(object):

    def __init__(self, email=None):
        
        self.email = str(email)
        
    def __repr__(self):
        
        return "{0}".format(self.email)
