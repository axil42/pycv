class Education(object):

    def __init__(self, year=None, title=None, degree=None, body_text=None):

        self.year = year
        self.title = title
        self.degree = degree
        self.body_text = body_text
